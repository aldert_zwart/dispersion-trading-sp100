# -*- coding: utf-8 -*-
"""
Created on Thu Feb 10 22:47:33 2022

@author: ajzwa
"""
from sklearn.decomposition import PCA
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from scipy import stats
import seaborn as sns

class PortfolioTracking:
    """
    the class PortfolioTracking mimics an index with a N number of its components
       
    Attributes
    ----------
        returns components
        returns index

    Methods
    ----------
        weights_df: gives a dataframe with weights of the tracking portfolio
    """
    def pca_plot(self):
        """
        calculates the principle component analysis of the covariance matrix of the index components
        
        Parameters
        -----------
        n_components, data
          
        Returns
        --------
        scatter plat with the pca analysis including the sector
        """
                
        p_components = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv",index_col=0)
        meta_data = pd.read_csv("../../data/components/metadata_oex.csv")
        #calculate the returns from the price
        returns_components = p_components.pct_change().dropna()
        returns_components = returns_components.cov()        
        #apply a 2 dimensional pca in order to able to plot a 2 dimensional graph with the results
        n_components=3  
        #perform the PCA
        X = returns_components.transpose()
        
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X) 
        df_components = pd.DataFrame(data=components,columns=['x', 'y', 'z'])
        df_components = df_components.set_index(returns_components.columns)
        #add meta data regarding the sector and marketCap
        
        meta_data=meta_data.set_index(meta_data.symbol)
        df_components = pd.merge(df_components, meta_data.sector, left_index=True, right_index=True)

        #group by sector in order to including the sectors in the graph
        groups = df_components.groupby('sector')
        # Plot
        sns.set(style = "darkgrid")
        
        fig = plt.figure(figsize=(10,14))
        ax = fig.add_subplot(projection = '3d')
        for name, group in groups:
            ax.plot(group.x, group.y, group.z, marker='o', linestyle='', ms=12, label=name, alpha=0.75)
        ax.legend(bbox_to_anchor=(1.35, 1))
        plt.title("Principle Component Analysis OEX")
        plt.savefig("../../Graphs/pca_oex.png", bbox_extra_artists=(ax,), bbox_inches='tight')
        return plt
    
    def pca_tracking_weights(self):
        """
        calculates the weights for every component in order to track the index
        
        Parameters
        -----------

          
        Returns
        --------
        DataFrame with the name & weights
        """
        #import the data
        p_components = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv",index_col=0)
        meta_data = pd.read_csv("../../data/components/metadata_oex.csv")
        #calculate the returns from the price
        returns_components = p_components.pct_change().dropna()
        returns_components = returns_components.cov()        
        #apply a 2 dimensional pca in order to able to plot a 2 dimensional graph with the results
        n_components=2  
        #perform the PCA
        X = returns_components.transpose()
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X) 
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components = df_components.set_index(returns_components.columns)
        #add meta data regarding the sector and marketCap
        
        meta_data=meta_data.set_index(meta_data.symbol)
        df_components = pd.merge(df_components, meta_data.ticker, left_index=True, right_index=True)
        #calculate the weights with the help of the explained variance
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        return df_components[['ticker',"weights"]]

        
    def pca_tracking_plot(self):
        """
        plots the tracking index and index and calculates the MSE
        
        Parameters
        -----------

          
        Returns
        --------
        mean squared error of the index and tracking index & plot with the returns of both the index & tracking index
        """
        #import the data
        data = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv", index_col=0)
        meta_data = pd.read_csv("../../Data/components/metadata_oex.csv")
        p_index = pd.read_csv("../../Data/price/price_oexYF.csv", index_col=0)
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_index=p_index.pct_change().dropna()
        returns_components_cov = returns_components.cov()  
        #perfrom the pca
        X = returns_components_cov.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components = df_components.set_index(returns_components.columns)
        #add meta data regarding the sector and marketCap
        
        meta_data=meta_data.set_index(meta_data.symbol)
        df_components = pd.merge(df_components, meta_data.ticker, left_index=True, right_index=True)
        
        #calculate the weights
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        tracking_index = weights.tolist()*returns_components
        portfolio_index = tracking_index.sum(axis=1)
        
        index_returns = returns_index[returns_index.index.isin(portfolio_index.index)]
        
        fig, ax = plt.subplots(figsize=(12,6))
        plt.title("Returns Tracking Index")
        ax.plot(portfolio_index,label='tracking portfolio')
        ax.plot(index_returns,label='index')
        ax.xaxis.set_ticklabels([])
        ax.legend() 
        #calculated MSE & t-test
        mse = mean_squared_error(index_returns,portfolio_index)
        plt.savefig("../../Graphs/portfolio_index_returns.png")
        return(print("Mean Sqaured error:",mse),plt.show())

    def cum_returns(self):
        """
        
        
        Parameters
        -----------

          
        Returns
        --------
        
        """
        #import the data
        data = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv", index_col=0)
        meta_data = pd.read_csv("../../Data/components/metadata_oex.csv")
        p_index = pd.read_csv("../../Data/price/price_oexYF.csv", index_col=0)
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_index=p_index.pct_change().dropna()
        returns_components_cov = returns_components.cov() 
        #perfrom the pca
        X = returns_components_cov.transpose()
        pca = PCA(n_components=2)
        components = pca.fit_transform(X)
        df_components = pd.DataFrame(data=components,columns=['x', 'y'])
        df_components["Name"]=meta_data.shortName
        #calculate the weights
        weights = abs(df_components['x'])/sum(abs(df_components['x']))
        df_components['weights'] = weights.tolist()
        tracking_index = weights.tolist()*returns_components
        portfolio_index = tracking_index.sum(axis=1)
        
        index_returns = returns_index[returns_index.index.isin(portfolio_index.index)]
        cum_index = (index_returns +1).cumprod()
        cum_portfolio = (portfolio_index+1).cumprod()
        
        fig, ax = plt.subplots(figsize=(12,6))
        plt.title("Cumulative Returns Tracking Portfolio & Index")
        ax.plot(cum_portfolio,label='tracking portfolio')
        ax.plot(cum_index,label='index')
        ax.xaxis.set_ticklabels([])
        ax.legend()     
        plt.savefig("../../Graphs/cum_returns_index_portfolio.png")
        t_test  = stats.ttest_ind(cum_portfolio,cum_index)

        return plt.show(), print("T-test",t_test)
    
    def top_n(self):
        """
        """
        #import the data
        data = pd.read_csv("../../Data/price/constituents_oex_cleansed.csv", index_col=0)
        meta_data = pd.read_csv("../../Data/components/metadata_oex.csv")
        #calculate the returns
        returns_components = data.pct_change().dropna() 
        returns_components_cov = returns_components.cov()  
        #perfrom the pca
        n_components=7
        X = returns_components_cov.transpose()
        pca = PCA(n_components=n_components)
        components = pca.fit_transform(X)
        joint_explaination = sum(pca.explained_variance_ratio_)
        
        weighted_arithmetic_mean= pca.explained_variance_ratio_/joint_explaination
        df_components = pd.DataFrame(data=components)
        df_components["Name"]=meta_data.ticker
        weighted_mean_corr = pd.DataFrame(
            data=[[np.corrcoef(returns_components_cov[c],
                               components[:,n])[1,0] for n in range(pca.n_components_)] 
                  for c in returns_components_cov], index=returns_components_cov.columns)
        
        df = ((weighted_mean_corr**2)*weighted_arithmetic_mean).sum(axis=1)
        df = df.sort_values(ascending=False)
        df = df.nlargest(40)
        return df
