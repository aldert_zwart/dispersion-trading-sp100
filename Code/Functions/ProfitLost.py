# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 17:06:04 2022

@author: ajzwa
"""
import numpy as np
import pandas as pd
import datetime
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import time

class ProfitLost:
    """
    The class ProfitLost caclulates the PnL of the dispersion trading strategy
    """
                  
        
    def trade_book_plain(self,enter, option_data, df_weights, df_price,date_lower_bound, date_higher_bound,list_symbols):
        """

        Parameters
        ----------
        enter : TYPE
            DESCRIPTION.
        option_data : TYPE
            DESCRIPTION.
        df_weights : TYPE
            DESCRIPTION.
        df_price : TYPE
            DESCRIPTION.
        date_lower_bound : TYPE
            DESCRIPTION.
        date_higher_bound : TYPE
            DESCRIPTION.
        list_symbols : TYPE
            DESCRIPTION.

        Returns
        -------
        trade_book_plain : TYPE
            DESCRIPTION.

        """
        trade_book_plain = pd.DataFrame()
        for row in range(len(enter)):
            for ticker in list_symbols:
                option_data_ticker = option_data[option_data.ticker==ticker] #get data of the ticker
                close_price_group = df_price.loc[enter.iloc[row].name] #get the close price of the group of asset for the strike price 
                close_price = close_price_group.loc[close_price_group.index == ticker] #get the close price for the individual asset            
                df_filtered_date=option_data_ticker.loc[option_data_ticker.date==enter.iloc[row].name]
                df_filtered_strike=df_filtered_date.loc[df_filtered_date.strike_price==int(round(close_price,-1)*1000)]
                df_filtered_maturity=df_filtered_strike.loc[(df_filtered_strike.exdate>enter.iloc[row].name+timedelta(days=date_lower_bound)) ]
                #strategy for the index
                if ticker == 'OEX':
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy OEX calls
                        df_filtered_cp =df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='C']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short                    
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
    
                        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is buy OEX puts
                        df_filtered_cp=df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='P']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
                        
                else:
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy consituents puts
                        df_filtered_cp =df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='P']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
    
                        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is buy constituents calls
                        df_filtered_cp =df_filtered_maturity.loc[df_filtered_maturity.cp_flag=='C']
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_plain=pd.concat([df_filtered_cp,trade_book_plain],ignore_index=True)
    
                        
        return trade_book_plain
    
    def get_exit_date_opposite_trade(self, enter):
        """
        

        Parameters
        ----------
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        df_exit_date : TYPE
            DESCRIPTION.

        """

        change=[]
        df_exit_date=pd.DataFrame()
        i=0
        exit_date=[]
        
        for row in range(len(enter)):
            try:
                if enter.iloc[row].trade != enter.iloc[row+1].trade:
                    change.append(enter.iloc[row+1].name)
            except:
                print('End of Loop')
        
 
        try:
            for row in range(len(enter)):
                exit_date.append(change[i])
                for row2 in range(len(change)):
                    if enter.iloc[row].name==change[row2]:
                        i+=1
        
        except:
            print()
        exit_date.pop(0)
        difference = len(enter)-len(exit_date)
        for _ in range(difference):
            exit_date.append(np.nan)
        
        df_exit_date=enter
        df_exit_date['exit_date']=exit_date 
        return df_exit_date
    
    def returns_portfolio(self, df_portfolio, enter, list_symbols, weights):
        """
        

        Parameters
        ----------
        df_portfolio : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.
        list_symbols : TYPE
            DESCRIPTION.
        weights : TYPE
            DESCRIPTION.

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        profit_lost=pd.DataFrame()
        for row in range(len(enter)):
            try:
                test=pd.DataFrame()
                df_filter_onEntryDate=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
                    test[security] =df.best_bid.tolist()
                    test.index=df.date
                test= test.replace(0, 0.01)
                returns_df= test.pct_change()
                returns_df= returns_df.iloc[0: , :]
                returns_df= returns_df.fillna(0.01)
                returns_df= returns_df*weights.weights.tolist()
                returns_df= returns_df.sum(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
        return profit_lost.groupby(profit_lost.index).mean()

    def delta_hedged_returns(self, df_portfolio, enter, list_symbols, weights, df_price):
        profit_lost=pd.DataFrame()
        for row in range(len(enter)):
            try:
                test=pd.DataFrame()
                df_filter_onEntryDate=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
                    test[security] =df.best_bid.tolist()
                    test.index=df.date
                test= test.replace(0, 0.01)
                returns_df= test.pct_change()
                returns_df= returns_df.iloc[0: , :]
                returns_df= returns_df.fillna(0.01)
                returns_df= returns_df*weights.weights.tolist()
                returns_df= returns_df.sum(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
        return profit_lost.groupby(profit_lost.index).mean()
    
    def returns_portfolio_delta(self,df_portfolio, enter, list_symbols,weights,df_price):
        profit_lost=pd.DataFrame()
        
    
        for row in range(len(enter)):
            df_options_price=pd.DataFrame()
            df_equity_price=pd.DataFrame()
            df_delta=pd.DataFrame()
            df_equity_price=pd.DataFrame()
            price=pd.DataFrame()
            try:
                df_filter_onEntryDate=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                for security in list_symbols:
                    
                    #get data per ticker and entery moment
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
    
                    #get option prices
                    df_options_price[security]=df.best_bid
                    df_options_price.index=df.date
    
                    #get the delta position
                    df_delta[security]=df.delta
                    df_delta.index=df.date
    
                    #get the price of the underlying asset 
                    df_equity_price=df_price[security]
                    df_equity_price=df_equity_price.loc[df_equity_price.index>=(df_filter_onEntryDate.date.min())]
                    df_equity_price=df_equity_price.loc[df_equity_price.index<=df_filter_onEntryDate.date.max()]
                    price[security]=df_equity_price
                    price.index=df_equity_price.index
    
                #caclulate options returns
                df_options_price=df_options_price.replace(0, 0.01)
                returns_df=df_options_price.pct_change()
                returns_df=returns_df.iloc[0:,:]
                returns_df=returns_df.fillna(0)
                returns_df=returns_df*weights.weights.tolist()
                returns_df=returns_df.mean(axis=1)
    
                #calculate delta hedging returns
                price=price.replace(0,0.01)
                df_equity_returns=price.pct_change()
                df_delta_returns=df_equity_returns*df_delta
                df_delta_returns=df_delta_returns.iloc[0:,:]
                df_delta_returns=df_delta_returns.fillna(0)
                df_delta_returns=df_delta_returns*weights.weights.tolist()
                df_delta_returns=df_delta_returns.mean(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df,df_delta_returns])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
            
        return profit_lost.groupby(profit_lost.index).mean()
    
    def trade_book_straddle(self,enter, option_data, df_weights, df_price,date_lower_bound, date_higher_bound,list_symbols):
        """
        

        Parameters
        ----------
        enter :             Pandas DataFrame
            Enter Signals of the indicator.
        option_data :       Pandas DataFrame
            Options Data Retrieved from OptionMetrics.
        df_weights :        Pandas DataFrame
            Calculated weights.
        df_price :          Pandas DataFrame
            timeseries of the assets.
        date_lower_bound :  Pandas DataFrame
            Range for excercise time T.
        date_higher_bound : Pandas DataFrame
            Range for excercise time T.
        list_symbols :      list
            All symbols used in the calculations.

        Returns
        -------
        trade_book_straddle : Pandas DataFrame
            Searches for all possible trades with pre-defined requirements

        """
        begin=time.time()#calculate begin time for the duration of the function
        trade_book_straddle = pd.DataFrame()#create df
        df_filtered_maturity = pd.DataFrame()#create df
        for row in range(len(enter)):#filter trough all the entry moments
            for ticker in list_symbols:#loop trough the tickers
                option_data_ticker = option_data[option_data.ticker==ticker] #get data of the ticker
                close_price_group = df_price.loc[enter.iloc[row].name] #get the close price of the group of asset for the strike price 
                close_price = close_price_group.loc[close_price_group.index == ticker] #get the close price for the individual asset            
                df_filtered_date=option_data_ticker.loc[option_data_ticker.date==enter.iloc[row].name]
                location = self.find_neighbours(close_price, df_filtered_date, 'strike_price')
                df_filtered_strike=df_filtered_date.loc[df_filtered_date.strike_price==int(df_filtered_date.loc[location[0]])]
                df_filtered_maturity=df_filtered_strike.loc[(df_filtered_strike.exdate>enter.iloc[row].name+timedelta(days=date_lower_bound)) ]

                #strategy for the index
                if ticker == 'OEX' or ticker == 'SPX':#condition on index
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy OEX straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=1#assign selling or buying straddles
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short 
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is selling OEX straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=-1#assign selling or buying straddles
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
                else:
                    if enter.iloc[row].trade == 'Short':
                        #Short dispersion is buy consituents straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=-1#assign selling or buying straddles
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)
        
        
                    if enter.iloc[row].trade == 'Long':
                        #long dispersion is selling constituents straddles
                        df_filtered_cp=df_filtered_maturity
                        df_filtered_cp['straddle_weights']=1#assign selling or buying straddles
                        df_filtered_cp['EntryDate']=enter.iloc[row].name #write the entry date
                        df_filtered_cp['TypeDispersion']=enter.iloc[row].trade #write the dispersion strategy long/short  
                        trade_book_straddle=pd.concat([df_filtered_cp,trade_book_straddle],ignore_index=True)

        duration= time.strftime("%H:%M:%S",(time.time()-begin))
        print(f"Done running, total time spend: {duration}")
        return trade_book_straddle
    
    def returns_portfolio_straddle(self,df_portfolio_straddle, enter, list_symbols,weights):
        profit_lost=pd.DataFrame()
        for row in range(len(enter)):
            try:
                df_filtered=pd.DataFrame()
                df_filter_onEntryDate=df_portfolio_straddle.loc[df_portfolio_straddle.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
                print(len(df_filter_onEntryDate))
                for security in list_symbols:
                    df = df_filter_onEntryDate.loc[df_filter_onEntryDate.ticker==security]
                    df_filtered[security]=(df.best_bid*df.straddle_weights).values            
                df_filtered.index=df.date
                df_filtered=df_filtered.groupby(df_filtered.index).mean()
                df_filtered= df_filtered.replace(0, 0.01)
                df_filtered= df_filtered.replace(-0.0,0.1)
                returns_df= df_filtered.pct_change()
                returns_df= returns_df.iloc[0:,:]
                returns_df= returns_df.fillna(0)
                returns_df= returns_df*weights.weights.tolist()
                returns_df= returns_df.mean(axis=1)
                profit_lost=pd.concat([profit_lost,returns_df])
            except:
                print(f"error in data with entry {enter.iloc[row].name}, missing data of: {security}")
        return profit_lost.groupby(profit_lost.index).sum()

    def exit_trade(self,trade_book1,enter):
        """
        

        Parameters
        ----------
        trade_book1 : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        trade_book1 : TYPE
            DESCRIPTION.

        """
        change=[]
        df_exit_date=pd.DataFrame()
        _=[]
        for row in range(len(enter)):
            try:
                if enter.iloc[row].trade != enter.iloc[row+1].trade:
                    change.append(enter.iloc[row+1].name)
            except:
                print('End of Loop')
                
    
        i=0
        exit_date=[]
        try:
            for row in range(len(enter)):
                exit_date.append(change[i])
                for row2 in range(len(change)):
                    if enter.iloc[row].name==change[row2]:
                        i+=1
    
        except:
            print()
        exit_date.pop(0)
        difference = len(enter)-len(exit_date)
        for _ in range(difference):
            exit_date.append(np.nan)
    
        df_exit_date=enter
        df_exit_date['exit_date']=exit_date 
        _=[]
        for row in range(len(trade_book1)):
            for row_1 in range(len(df_exit_date)):
                if trade_book1.date.loc[row] == df_exit_date.iloc[row_1].name:
                    _.append(df_exit_date.iloc[row_1].exit_date)
        trade_book1['exit_date_opposite_trade']=_    
        trade_book1['exit_date_maturity']=trade_book1.exdate-timedelta(days=5)
        
        list_exit=[]
        for row in range(len(trade_book1)):
            list_exit.append(min(trade_book1.loc[row].exit_date_maturity,trade_book1.loc[row].exit_date_opposite_trade))
        trade_book1['exit_date']=list_exit
        trade_book1['bid_ask_spread']=trade_book1.best_offer-trade_book1.best_bid
        trade_book1.to_csv('../../Data/trades.csv')
    
        return trade_book1
    
    def portfolio(self,trade_book1,option_data):
        """
        

        Parameters
        ----------
        trade_book1 : TYPE
            DESCRIPTION.

        Returns
        -------
        df_portfolio : TYPE
            DESCRIPTION.

        """
        df_portfolio = pd.DataFrame()
        for row in range(len(trade_book1)):
            optionid = trade_book1.optionid.loc[row]
            begin_date = trade_book1.date.loc[row]
            end_date = trade_book1.exit_date.loc[row]
            df=option_data.loc[option_data.optionid==optionid]
            df=df.loc[option_data.date>begin_date]
            df=df.loc[df.date<end_date]
            df['EntryDate']=begin_date
            df_portfolio= pd.concat([df_portfolio,df])
    
        return df_portfolio
    
    def filter_highest_maturity(self,df_trade_book,enter):
        trade_book1 = pd.DataFrame()
        for row in range(len(enter)):
            trade_book_perentry=df_trade_book.loc[df_trade_book.date==enter.iloc[row].name]
            highest_date=trade_book_perentry.exdate.max()
            df=trade_book_perentry.loc[trade_book_perentry.exdate==highest_date]
            _ = df.drop_duplicates(subset=['optionid'])
            trade_book1=pd.concat([trade_book1,_],ignore_index=True)
        return trade_book1
    
    def filter_maturity(self,trade_book_plain,enter, list_symbols):
        """
        

        Parameters
        ----------
        trade_book_plain : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        trade_book : TYPE
            DESCRIPTION.

        """
        trade_book = pd.DataFrame()
        for row in range(len(enter)):
            trade_book_perentry = trade_book_plain.loc[trade_book_plain.date==enter.iloc[row].name]
            for date in trade_book_perentry.exdate.tolist():
                if len(trade_book_perentry.ticker.loc[trade_book_perentry.exdate==date])==len(list_symbols):
                    _ = trade_book_perentry.loc[trade_book_perentry.exdate==date]
                    trade_book = pd.concat([trade_book,_],ignore_index=True)
        return trade_book
  
    def missing_values(self,enter, df_portfolio,list_symbols):
      missing_values={'security': [], 'enter': [], 'date':[]}
      for row in range(len(enter)):
          df=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
          for date in df.date.unique():
              df=df_portfolio.loc[df_portfolio.EntryDate==enter.iloc[row].name.replace(tzinfo=None)]
              df=df.loc[df.date==date]
              if len(df.ticker.tolist())!=len(list_symbols):
                  for security in list_symbols:
                      if security not in df.ticker.tolist():
                          missing_values['security'].append(security)
                          missing_values['enter'].append(enter.iloc[row].name)
                          missing_values['date'].append(date)
                          missing_values['optionid'].append(df.optionid)
      return pd.DataFrame(missing_values)  
  
    def filter_maturity_straddle(self,trade_book_plain,enter, list_symbols):
        """
            
    
        Parameters
        ----------
        trade_book_plain : TYPE
            DESCRIPTION.
        enter : TYPE
            DESCRIPTION.
    
        Returns
        -------
        trade_book : TYPE
            DESCRIPTION.
    
        """
        trade_book = pd.DataFrame()
        dates=[]
        for row in range(len(enter)):
            trade_book_perentry = trade_book_plain.loc[trade_book_plain.date==enter.iloc[row].name]
            for date in trade_book_perentry.exdate.tolist():
                if len(trade_book_perentry.ticker.loc[(trade_book_perentry.exdate==date) & (trade_book_perentry.cp_flag=='C')])==len(list_symbols) and len(trade_book_perentry.ticker.loc[(trade_book_perentry.exdate==date) & (trade_book_perentry.cp_flag=='P')])==len(list_symbols) :
                    dates.append(date)
            _ = trade_book_perentry.loc[trade_book_perentry.exdate==max(dates)]
            trade_book = pd.concat([trade_book,_],ignore_index=True)
        return trade_book
    
    def portfolio_straddle(self,trade_book1,option_data):
        """
    
    
        Parameters
        ----------
        trade_book1 : TYPE
            DESCRIPTION.
    
        Returns
        -------
        df_portfolio : TYPE
            DESCRIPTION.
    
        """
        df_portfolio = pd.DataFrame()
        for row in range(len(trade_book1)):
            straddle_weights = trade_book1.straddle_weights.loc[row]
            optionid = trade_book1.optionid.loc[row]
            begin_date = trade_book1.date.loc[row]
            end_date = trade_book1.exit_date.loc[row]
            df=option_data.loc[option_data.optionid==optionid]
            df=df.loc[df.date>begin_date]
            df=df.loc[df.date<end_date]
            df['EntryDate']=begin_date
            df['optionid']=optionid
            df['straddle_weights']=straddle_weights
            df_portfolio= pd.concat([df_portfolio,df])
    
        return df_portfolio
    
    def find_neighbours(self, value, df, colname):
        exactmatch = df[df[colname] == value]
        if not exactmatch.empty:
            return exactmatch.index
        else:
            lowerneighbour_ind = df[df[colname] < value][colname].idxmax()
            upperneighbour_ind = df[df[colname] > value][colname].idxmin()
        return [lowerneighbour_ind, upperneighbour_ind]
    
    
    
class Rebalancing:
    """
    class for rebalancing for a delta neutral trategy
    """
    def rebalance(entry,options_data,options_price, list_symbols, price):
        
        
        
        df=0
        return df
        
