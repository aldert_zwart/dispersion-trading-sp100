# -*- coding: utf-8 -*-
"""
Created on Thu Feb 24 14:17:04 2022

@author: ajzwa


"""
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np
import matplotlib.dates as mdates
import datetime

class VolatilityCorrelation:
    """
    The class VolatilityCorrelation calculates the volatility of the index & the tracking portfolio.
       
    """
    
    def dispersion(self, oex_imv, constituents_imv, weights):
        """
        This function calculates the dispers 

        Parameters
        ----------
        oex_imv : TYPE
            Index implied volatility.
        constituents_imv : TYPE
            individual assets implied volatility.
        weights : weights of the individual assets
            DESCRIPTION.

        Returns
        -------
        dispersion : TYPE
            DESCRIPTION.

        """
        oex_imv=oex_imv.squeeze()
        weighted_imv_oex = (constituents_imv*weights).sum(axis=1)
        dispersion = weighted_imv_oex**2-oex_imv**2
        return dispersion
        

    def implied_correlation(self,index_ivol,portfolio_ivol,weights):
        """
        This function calculates the implied correlation
    
        Parameters
        ----------
        index_ivol : pandas dataframe
            Implied volatility of the index.
        portfolio_ivol : pandas dataframe
            Implied volatility of the individual assets.
        weights : pandas series
            Weights of the portfolio.
    
        Returns
        -------
        implied_cor : Pandas Series
            30 Day Implied correlation.
    
        """
        weights=np.array(weights)
        wi = weights
        wj = weights.T
        wi = wi.tolist()
        wj = wj.tolist()
        portfolio_ivol=(portfolio_ivol/100)
        sigma_i = portfolio_ivol
        df_implied_corr=pd.DataFrame()
        try:
            len(sigma_i.index)==len(index_ivol.index)
        except:
            print("Dataframes do not align in dates")
        for row in range(len(sigma_i)):
            sigma_i_row = sigma_i.iloc[row]#iterate over sigma_i
            sigma_j_row = sigma_i_row.transpose()#get sigma_j, the transpose of sigma_i 
            index_ivol_row = index_ivol.iloc[row]#interate over the ivol of the index
            index_ivol_row=(index_ivol_row/100)
            pairwise_weighted_iv = (np.dot(wi,np.dot(wj,np.dot(sigma_i_row,sigma_j_row)))).sum()
            pairwise_weighted_iv = 2*pairwise_weighted_iv
            index_variance = (index_ivol_row**2)
            portfolio_variance = np.dot(((pd.Series(wi))**2),(sigma_i_row**2))
            implied_corr = (index_variance - portfolio_variance)/pairwise_weighted_iv
            df_temp = pd.DataFrame()
            df_temp['implied_corr']=implied_corr.values
            df_temp['Date']=implied_corr.name
            df_implied_corr=df_implied_corr.append(df_temp)
        return df_implied_corr


    def sample_correlation(self, returns_constituents, portfolio_ivol,weights):
        """
        This function calculates the sample correlation according to the formula
        given by Ferrari et al 2020        
    
        Parameters
        ----------
        returns_constituents : Pandas dataframe
            DESCRIPTION.
        portfolio_ivol : Pandas dataframe
            DESCRIPTION.
        weights : Pandas dataframe
            DESCRIPTION.
    
        Returns
        -------
        sample_correlation : Pandas dataserie
            DESCRIPTION.
    
        """
        corr = returns_constituents.rolling(30).corr(pairwise=True).dropna()
        cov =  returns_constituents.rolling(30).cov(pairwise=True).dropna()
        corr = corr.loc[portfolio_ivol.index[0]:portfolio_ivol.index[-1]]
        cov = cov.loc[portfolio_ivol.index[0]:portfolio_ivol.index[-1]]
        weights=np.array(weights)
        wi = weights
        wj = weights.T
        wi = wi.tolist()
        wj = wj.tolist()
        sigma_i = portfolio_ivol
        df_implied_corr_sample=pd.DataFrame()
        try:
            len(sigma_i.index)==len(corr.index)
        except:
            print("Dataframes do not align in dates")
        for row in range(len(sigma_i)):
            sigma_i_row = sigma_i.iloc[row]/100#iterate over sigma_i
            sigma_j_row = sigma_i_row.transpose()/100#get sigma_j, the transpose of sigma_i 
            date = sigma_i.iloc[row].name
            cov_row1=sigma_i_row.cov(sigma_i_row)
            cov_row = cov.loc[date]
            corr_row = corr.loc[date]
            numerator=(np.dot(wi,np.dot(wj,np.dot(sigma_i_row,np.dot(sigma_j_row,corr_row))))).sum()
            denomirator=(np.dot(wi,np.dot(wj,np.dot(sigma_i_row,sigma_j_row)))).sum()
            numerator=np.dot(wi,np.dot(wj,cov_row1)).sum()
            denomirator=(np.dot(wi,np.dot(wj,np.dot(sigma_j_row,sigma_i_row)))).sum()
            sample_correlation=numerator/denomirator
            df_temp = pd.DataFrame()
            df_temp['implied_corr']=pd.Series(sample_correlation)
            df_temp['Date']=pd.Series(date)
            df_implied_corr_sample=df_implied_corr_sample.append(df_temp)
            
        return df_implied_corr_sample

    def portfolio_variance(self,returns_constituents,df_weights):
        portfolio_variance=pd.DataFrame() 
        returns_constituents=returns_constituents[df_weights.index.tolist()]
        wi=df_weights.coef
        wj=wi.T
        cov = returns_constituents.rolling(30).cov(pairwise=True).dropna()
        for date in cov.index.unique("Date"):
            _=np.dot(wj,np.dot(cov.loc[date],wi))
            df=pd.DataFrame({"Date":[date],"Variance":[_],"Volatility":np.sqrt(_)*100})
            portfolio_variance=portfolio_variance.append(df)
        return portfolio_variance 
    
    def index_volatility(self,price_oex):
        """
        

        Parameters
        ----------
        price_oex : Pandas DataFrame
            DESCRIPTION.

        Returns
        -------
        Pandas DataFrame
            Returns the index volatility.

        """
        return price_oex.rolling(30).CLOSE.std().dropna()
       
    def pairwise_corr_signal(self,df_imv_oex,df_weights,returns_constituents):
        pairwise_corr_signal=pd.DataFrame()
        returns_constituents=returns_constituents[df_weights.index.tolist()]
        std=returns_constituents.rolling(30).std(pairwise=True).dropna()
        for row in range(len(std)):
            std.index=pd.to_datetime(std.index)
            x=df_imv_oex[df_imv_oex.index.date==std.iloc[row].name]
            _ = np.dot(df_weights.coef,np.dot(std.iloc[row],np.dot(std.iloc[row].T,df_weights.coef.T)))
            df=pd.DataFrame({"Date":[std.iloc[row].name],"signal":[_]})
            pairwise_corr_signal=pairwise_corr_signal.append(df)
        return pairwise_corr_signal
    
    def indicator(self, implied_correlation, sample_correlation):
        
        indicator = implied_correlation - sample_correlation
        return indicator
    